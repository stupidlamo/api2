###создаем пипелину
NEW_API1_PIPE=curl --request POST --form token=$API1_PIP_TOKEN --form ref=Dev --form "variables[WEB_IMG]=true" --form "variables[NGINX_IMG]=true" --form "variables[PORT_NGINX]=true" https://gitlab.com/api/v4/projects/22610249/trigger/pipeline | jq '.id'

### ищем джобу
JOBID=curl --header "PRIVATE-TOKEN: $MY_TOKEN" "https://gitlab.com/api/v4/projects/22610249/pipelines/$NEW_API1_PIPE/jobs?scope[]=skipped" | jq .[].id

### стартуем джобу
curl --request POST --header "PRIVATE-TOKEN: $MY_TOKEN" "https://gitlab.com/api/v4/projects/22610249/jobs/$JOBID/play"
