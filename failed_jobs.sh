#!/bin/bash
echo "This script will get last failed job from current project"
echo "NB! PROJECT ID IS !NOT! VARIABLE!"
sleep 5
curl --header "PRIVATE-TOKEN: iKa2kHST1RCzRnmGTvvZ" "https://gitlab.com/api/v4/projects/22610538/jobs?scope[]=failed" | jq '.[0] | select(.name == "deploy_master")'
echo "###END OF SCRIPT###"

