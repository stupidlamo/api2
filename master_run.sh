#!/bin/sh
###создаем пипелину
NEW_API1_PIPE=$(curl --request POST --form token=$API1_PIP_TOKEN --form ref=master --form "variables[WEB_IMG]=$WEB_IMG" --form "variables[NGINX_IMG]=$NGINX_IMG" --form "variables[PORT_NGINX]=$PORT_NGINX" https://gitlab.com/api/v4/projects/22610249/trigger/pipeline | jq '.id')
sleep 15
### ищем джобу
JOBID=$(curl --header "PRIVATE-TOKEN: $MY_TOKEN" "https://gitlab.com/api/v4/projects/22610249/pipelines/$NEW_API1_PIPE/jobs" | jq .[].id)
sleep 15
### стартуем джобу
curl --request POST --header "PRIVATE-TOKEN: $MY_TOKEN" "https://gitlab.com/api/v4/projects/22610249/jobs/$JOBID/play"
